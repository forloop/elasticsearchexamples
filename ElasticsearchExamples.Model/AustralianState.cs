namespace ElasticsearchExamples.Model
{
    public enum AustralianState
    {
        NSW = 1,

        VIC = 2,

        QLD = 3,

        SA = 4,

        WA = 5,

        TAS = 6,

        NT = 7,

        ACT = 8
    }
}