using ElasticsearchExamples.Model;
using Nest;

namespace ElasticsearchExamples.Indexer
{
    public static class SuburbMapping
    {
        public static CreateIndexDescriptor CreateSuburbMapping(CreateIndexDescriptor indexDescriptor, string index)
        {
            return indexDescriptor
                .Index(index)
                .AddMapping<Suburb>(mappingDescriptor => mappingDescriptor
                    .MapFromAttributes()
                    .Properties(p => p
                        .GeoShape(g => g
                            .Name(s => s.Geometry)
                            .TreeLevels(10)
                        )
                    )
                );
        }
    }
}